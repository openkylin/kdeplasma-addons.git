# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Alexander Potashev <aspotashev@gmail.com>, 2010, 2011, 2016, 2019.
# Alexander Yavorsky <kekcuha@gmail.com>, 2018, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-13 00:44+0000\n"
"PO-Revision-Date: 2021-12-28 15:10+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.0\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: spellcheck.cpp:96 spellcheck_config.cpp:66 spellcheck_config.cpp:97
#, kde-format
msgid "spell"
msgstr "орфо"

#: spellcheck.cpp:101
#, kde-format
msgctxt ""
"Spelling checking runner syntax, first word is trigger word, e.g.  \"spell\"."
msgid "%1:q:"
msgstr "%1:q:"

#: spellcheck.cpp:102
#, kde-format
msgid "Checks the spelling of :q:."
msgstr "Проверка орфографии в :q:."

#: spellcheck.cpp:209
#, kde-format
msgctxt "Term is spelled correctly"
msgid "Correct"
msgstr "Нет орфографических ошибок"

#: spellcheck.cpp:218
#, kde-format
msgid "Suggested term"
msgstr "Возможные слова"

#: spellcheck.cpp:227
#, kde-format
msgid "No dictionary found, please install hspell"
msgstr "Словари не найдены, установите пакет hspell"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: spellcheck_config.ui:17
#, kde-format
msgid "Spell Check Settings"
msgstr "Настройка проверки орфографии"

#. i18n: ectx: property (text), widget (QCheckBox, m_requireTriggerWord)
#: spellcheck_config.ui:23
#, kde-format
msgid "&Require trigger word"
msgstr "&Проверять только при наличии ключевого слова"

#. i18n: ectx: property (text), widget (QLabel, label)
#: spellcheck_config.ui:32
#, kde-format
msgid "&Trigger word:"
msgstr "&Ключевое слово:"

#. i18n: ectx: property (text), widget (QPushButton, m_openKcmButton)
#: spellcheck_config.ui:62
#, kde-format
msgid "Configure Dictionaries…"
msgstr "Настроить словари…"