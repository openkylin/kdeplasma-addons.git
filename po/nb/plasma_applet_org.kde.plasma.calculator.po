# Translation of plasma_applet_org.kde.plasma.calculator to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2009, 2010, 2012.
msgid ""
msgstr ""
"Project-Id-Version: KDE 4\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-09-25 02:25+0200\n"
"PO-Revision-Date: 2012-12-30 22:11+0100\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.4\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: package/contents/ui/calculator.qml:290
#, kde-format
msgctxt "Text of the clear button"
msgid "C"
msgstr "C"

#: package/contents/ui/calculator.qml:298
#, kde-format
msgctxt "Text of the division button"
msgid "÷"
msgstr "/"

#: package/contents/ui/calculator.qml:306
#, kde-format
msgctxt "Text of the multiplication button"
msgid "×"
msgstr "×"

#: package/contents/ui/calculator.qml:314
#, kde-format
msgctxt "Text of the all clear button"
msgid "AC"
msgstr "AC"

#: package/contents/ui/calculator.qml:347
#, kde-format
msgctxt "Text of the minus button"
msgid "-"
msgstr "-"

#: package/contents/ui/calculator.qml:381
#, kde-format
msgctxt "Text of the plus button"
msgid "+"
msgstr "+"

#: package/contents/ui/calculator.qml:416
#, kde-format
msgctxt "Text of the equals button"
msgid "="
msgstr "="