# Translation of plasma_applet_timer into esperanto.
# Axel Rousseau <axel@esperanto-jeunes.org>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_timer\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-03-02 03:07+0100\n"
"PO-Revision-Date: 2009-11-15 12:06+0100\n"
"Last-Translator: Axel Rousseau <axel@esperanto-jeunes.org>\n"
"Language-Team: esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: pology\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:13
#, fuzzy, kde-format
#| msgid "Appearance"
msgctxt "@title"
msgid "Appearance"
msgstr "Aspekto"

#: package/contents/config/config.qml:19
#, kde-format
msgctxt "@title"
msgid "Predefined Timers"
msgstr ""

#: package/contents/config/config.qml:24
#, kde-format
msgctxt "@title"
msgid "Advanced"
msgstr ""

#: package/contents/ui/configAdvanced.qml:22
#, kde-format
msgctxt "@title:label"
msgid "After timer completes:"
msgstr ""

#: package/contents/ui/configAdvanced.qml:26
#, kde-format
msgctxt "@option:check"
msgid "Execute command:"
msgstr ""

#: package/contents/ui/configAppearance.qml:27
#, kde-format
msgctxt "@title:label"
msgid "Display:"
msgstr ""

#: package/contents/ui/configAppearance.qml:33
#, kde-format
msgctxt "@option:check"
msgid "Show title:"
msgstr ""

#: package/contents/ui/configAppearance.qml:50
#, kde-format
msgctxt "@option:check"
msgid "Show seconds"
msgstr ""

#: package/contents/ui/configAppearance.qml:62
#, kde-format
msgctxt "@title:label"
msgid "Notifications:"
msgstr ""

#: package/contents/ui/configAppearance.qml:66
#, kde-format
msgctxt "@option:check"
msgid "Show notification text:"
msgstr ""

#: package/contents/ui/configTimes.qml:76
#, kde-format
msgid ""
"If you add predefined timers here, they will appear in plasmoid context menu."
msgstr ""

#: package/contents/ui/configTimes.qml:83
#, kde-format
msgid "Add"
msgstr ""

#: package/contents/ui/configTimes.qml:120
#, kde-format
msgid "Scroll over digits to change time"
msgstr ""

#: package/contents/ui/configTimes.qml:126
#, kde-format
msgid "Apply"
msgstr ""

#: package/contents/ui/configTimes.qml:134
#, kde-format
msgid "Cancel"
msgstr ""

#: package/contents/ui/configTimes.qml:143
#, kde-format
msgid "Edit"
msgstr ""

#: package/contents/ui/configTimes.qml:152
#, kde-format
msgid "Delete"
msgstr ""

#: package/contents/ui/main.qml:40 package/contents/ui/main.qml:101
#, kde-format
msgid "Timer"
msgstr ""

#: package/contents/ui/main.qml:45
#, kde-format
msgid "%1 is running"
msgstr ""

#: package/contents/ui/main.qml:47
#, kde-format
msgid "%1 not running"
msgstr ""

#: package/contents/ui/main.qml:51
#, kde-format
msgid "Remaining time left: %1 second"
msgid_plural "Remaining time left: %1 seconds"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Timer finished"
msgstr ""

#: package/contents/ui/main.qml:119
#, fuzzy, kde-format
#| msgid "Start"
msgctxt "@action"
msgid "&Start"
msgstr "Lanĉi"

#: package/contents/ui/main.qml:120
#, kde-format
msgctxt "@action"
msgid "S&top"
msgstr ""

#: package/contents/ui/main.qml:121
#, fuzzy, kde-format
#| msgid "Reset"
msgctxt "@action"
msgid "&Reset"
msgstr "Restarigu"

#: package/contents/ui/TimerView.qml:72
#, kde-format
msgid "Timer is running"
msgstr ""

#: package/contents/ui/TimerView.qml:72
#, kde-format
msgid ""
"Use mouse wheel to change digits or choose from predefined timers in the "
"context menu"
msgstr ""