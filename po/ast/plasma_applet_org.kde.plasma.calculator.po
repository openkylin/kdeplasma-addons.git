# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# enolp <enolp@softastur.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-09-25 02:25+0200\n"
"PO-Revision-Date: 2019-09-07 13:33+0200\n"
"Last-Translator: enolp <enolp@softastur.org>\n"
"Language-Team: Asturian <alministradores@softastur.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.08.0\n"

#: package/contents/ui/calculator.qml:290
#, kde-format
msgctxt "Text of the clear button"
msgid "C"
msgstr "C"

#: package/contents/ui/calculator.qml:298
#, kde-format
msgctxt "Text of the division button"
msgid "÷"
msgstr "÷"

#: package/contents/ui/calculator.qml:306
#, kde-format
msgctxt "Text of the multiplication button"
msgid "×"
msgstr "×"

#: package/contents/ui/calculator.qml:314
#, kde-format
msgctxt "Text of the all clear button"
msgid "AC"
msgstr "AC"

#: package/contents/ui/calculator.qml:347
#, kde-format
msgctxt "Text of the minus button"
msgid "-"
msgstr "-"

#: package/contents/ui/calculator.qml:381
#, kde-format
msgctxt "Text of the plus button"
msgid "+"
msgstr "+"

#: package/contents/ui/calculator.qml:416
#, kde-format
msgctxt "Text of the equals button"
msgid "="
msgstr "="