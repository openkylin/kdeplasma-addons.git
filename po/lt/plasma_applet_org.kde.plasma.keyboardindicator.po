# Lithuanian translations for kdeplasma-addons package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-04-08 00:20+0000\n"
"PO-Revision-Date: 2021-08-18 01:10+0300\n"
"Last-Translator: Moo\n"
"Language-Team: lt\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.0\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Keys"
msgstr "Klavišai"

#: contents/ui/configAppearance.qml:32
#, kde-format
msgctxt "@option:check"
msgid "Caps Lock"
msgstr "Didžiosios raidės"

#: contents/ui/configAppearance.qml:39
#, kde-format
msgctxt "@option:check"
msgid "Num Lock"
msgstr "Skaitmeninė klaviatūra"

#: contents/ui/main.qml:29
#, kde-format
msgid "Caps Lock activated\n"
msgstr "Įjungtos didžiosios raidės\n"

#: contents/ui/main.qml:30
#, kde-format
msgid "Num Lock activated\n"
msgstr "Įjungta skaitmeninė klaviatūra\n"

#: contents/ui/main.qml:101
#, kde-format
msgid "No lock keys activated"
msgstr "Neįjungti jokie užrakto klavišai"