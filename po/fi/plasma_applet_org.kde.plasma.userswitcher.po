# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Lasse Liehu <lasse.liehu@gmail.com>, 2015, 2016.
# Tommi Nieminen <translator@legisign.org>, 2018, 2019, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-13 00:44+0000\n"
"PO-Revision-Date: 2022-01-11 22:40+0200\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.2\n"

#: package/contents/config/config.qml:11
#, kde-format
msgctxt "@title"
msgid "General"
msgstr "Yleistä"

#: package/contents/ui/configGeneral.qml:27
#, kde-format
msgctxt "@title:label"
msgid "Username style:"
msgstr "Käyttäjätunnuksen tyyli:"

#: package/contents/ui/configGeneral.qml:30
#, kde-format
msgctxt "@option:radio"
msgid "Full name (if available)"
msgstr "Koko nimi (jos saatavilla)"

#: package/contents/ui/configGeneral.qml:37
#, kde-format
msgctxt "@option:radio"
msgid "Login username"
msgstr "Käyttäjätunnus"

#: package/contents/ui/configGeneral.qml:55
#, kde-format
msgctxt "@title:label"
msgid "Show:"
msgstr "Näytä:"

#: package/contents/ui/configGeneral.qml:58
#, kde-format
msgctxt "@option:radio"
msgid "Name"
msgstr "Nimi"

#: package/contents/ui/configGeneral.qml:72
#, kde-format
msgctxt "@option:radio"
msgid "User picture"
msgstr "Käyttäjän kuva"

#: package/contents/ui/configGeneral.qml:86
#, kde-format
msgctxt "@option:radio"
msgid "Name and user picture"
msgstr "Nimi ja käyttäjän kuva"

#: package/contents/ui/configGeneral.qml:105
#, kde-format
msgctxt "@title:label"
msgid "Advanced:"
msgstr "Lisäasetukset:"

#: package/contents/ui/configGeneral.qml:107
#, kde-format
msgctxt "@option:check"
msgid "Show technical session information"
msgstr "Näytä istunnon tekniset tiedot"

#: package/contents/ui/main.qml:41
#, kde-format
msgid "You are logged in as <b>%1</b>"
msgstr "Olet kirjautuneena käyttäjänä <b>%1</b>"

#: package/contents/ui/main.qml:146
#, kde-format
msgid "Current user"
msgstr "Nykyinen käyttäjä"

#: package/contents/ui/main.qml:169
#, kde-format
msgctxt "Nobody logged in on that session"
msgid "Unused"
msgstr "Käyttämätön"

#: package/contents/ui/main.qml:185
#, kde-format
msgctxt "User logged in on console number"
msgid "TTY %1"
msgstr "TTY %1"

#: package/contents/ui/main.qml:187
#, kde-format
msgctxt "User logged in on console (X display number)"
msgid "on %1 (%2)"
msgstr "näytöllä %1 (%2)"

#: package/contents/ui/main.qml:207
#, kde-format
msgctxt "@action"
msgid "New Session"
msgstr "Uusi istunto"

#: package/contents/ui/main.qml:216
#, kde-format
msgctxt "@action"
msgid "Lock Screen"
msgstr "Lukitse näyttö"

#: package/contents/ui/main.qml:226
#, kde-format
msgctxt "Show a dialog with options to logout/shutdown/restart"
msgid "Leave…"
msgstr "Poistu…"