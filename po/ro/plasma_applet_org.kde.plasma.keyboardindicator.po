# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# Sergiu Bivol <sergiu@cip.md>, 2020, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-04-08 00:20+0000\n"
"PO-Revision-Date: 2022-06-01 15:19+0100\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.3\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Keys"
msgstr "Taste"

#: contents/ui/configAppearance.qml:32
#, kde-format
msgctxt "@option:check"
msgid "Caps Lock"
msgstr "Caps Lock"

#: contents/ui/configAppearance.qml:39
#, kde-format
msgctxt "@option:check"
msgid "Num Lock"
msgstr "Num Lock"

#: contents/ui/main.qml:29
#, kde-format
msgid "Caps Lock activated\n"
msgstr "Caps Lock activat\n"

#: contents/ui/main.qml:30
#, kde-format
msgid "Num Lock activated\n"
msgstr "Num Lock activat\n"

#: contents/ui/main.qml:101
#, kde-format
msgid "No lock keys activated"
msgstr "Nicio tastă de blocare activă"