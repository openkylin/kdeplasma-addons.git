# translation of plasma_applet_notes.po to Latvian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Maris Nartiss <maris.kde@gmail.com>, 2007.
# Viesturs Zarins <viesturs.zarins@mii.lu.lv>, 2008.
# Viesturs Zariņš <viesturs.zarins@mii.lu.lv>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_notes\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-02-08 02:11+0000\n"
"PO-Revision-Date: 2009-06-03 20:40+0300\n"
"Last-Translator: Viesturs Zariņš <viesturs.zarins@mii.lu.lv>\n"
"Language-Team: Latvian <locale@laka.lv>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr ""

#: package/contents/ui/configAppearance.qml:31
#, kde-format
msgid "%1pt"
msgstr ""

#: package/contents/ui/configAppearance.qml:37
#, fuzzy, kde-format
#| msgid "Use custom font size:"
msgid "Text font size:"
msgstr "Lietot pielāgotu fonta izmēru:"

#: package/contents/ui/configAppearance.qml:42
#, kde-format
msgid "Background color"
msgstr ""

#: package/contents/ui/configAppearance.qml:78
#, kde-format
msgid "A white sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:79
#, kde-format
msgid "A black sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:80
#, kde-format
msgid "A red sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:81
#, kde-format
msgid "An orange sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:82
#, kde-format
msgid "A yellow sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:83
#, kde-format
msgid "A green sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:84
#, kde-format
msgid "A blue sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:85
#, kde-format
msgid "A pink sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:86
#, kde-format
msgid "A translucent sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:87
#, kde-format
msgid "A translucent sticky note with light text"
msgstr ""

#: package/contents/ui/main.qml:244
#, kde-format
msgid "Undo"
msgstr ""

#: package/contents/ui/main.qml:252
#, kde-format
msgid "Redo"
msgstr ""

#: package/contents/ui/main.qml:262
#, kde-format
msgid "Cut"
msgstr ""

#: package/contents/ui/main.qml:270
#, kde-format
msgid "Copy"
msgstr ""

#: package/contents/ui/main.qml:278
#, kde-format
msgid "Paste Without Formatting"
msgstr ""

#: package/contents/ui/main.qml:284
#, kde-format
msgid "Paste"
msgstr ""

#: package/contents/ui/main.qml:293
#, kde-format
msgid "Delete"
msgstr ""

#: package/contents/ui/main.qml:300
#, kde-format
msgid "Clear"
msgstr ""

#: package/contents/ui/main.qml:310
#, kde-format
msgid "Select All"
msgstr ""

#: package/contents/ui/main.qml:393
#, fuzzy, kde-format
#| msgid "Bold"
msgctxt "@info:tooltip"
msgid "Bold"
msgstr "Trekns"

#: package/contents/ui/main.qml:405
#, fuzzy, kde-format
#| msgid "Italic"
msgctxt "@info:tooltip"
msgid "Italic"
msgstr "Slīpraksts"

#: package/contents/ui/main.qml:417
#, fuzzy, kde-format
#| msgid "Underline"
msgctxt "@info:tooltip"
msgid "Underline"
msgstr "Pasvītrots"

#: package/contents/ui/main.qml:429
#, fuzzy, kde-format
#| msgid "StrikeOut"
msgctxt "@info:tooltip"
msgid "Strikethrough"
msgstr "Pārsvīrtrots"

#: package/contents/ui/main.qml:503
#, kde-format
msgid "Discard this note?"
msgstr ""

#: package/contents/ui/main.qml:504
#, kde-format
msgid "Are you sure you want to discard this note?"
msgstr ""

#: package/contents/ui/main.qml:517
#, fuzzy, kde-format
#| msgid "White"
msgctxt "@item:inmenu"
msgid "White"
msgstr "Balts"

#: package/contents/ui/main.qml:518
#, fuzzy, kde-format
#| msgid "Black"
msgctxt "@item:inmenu"
msgid "Black"
msgstr "Melns"

#: package/contents/ui/main.qml:519
#, fuzzy, kde-format
#| msgid "Red"
msgctxt "@item:inmenu"
msgid "Red"
msgstr "Sarkans"

#: package/contents/ui/main.qml:520
#, fuzzy, kde-format
#| msgid "Orange"
msgctxt "@item:inmenu"
msgid "Orange"
msgstr "Oranžs"

#: package/contents/ui/main.qml:521
#, fuzzy, kde-format
#| msgid "Yellow"
msgctxt "@item:inmenu"
msgid "Yellow"
msgstr "Dzeltens"

#: package/contents/ui/main.qml:522
#, fuzzy, kde-format
#| msgid "Green"
msgctxt "@item:inmenu"
msgid "Green"
msgstr "Zaļš"

#: package/contents/ui/main.qml:523
#, fuzzy, kde-format
#| msgid "Blue"
msgctxt "@item:inmenu"
msgid "Blue"
msgstr "Zils"

#: package/contents/ui/main.qml:524
#, fuzzy, kde-format
#| msgid "Pink"
msgctxt "@item:inmenu"
msgid "Pink"
msgstr "Rozā"

#: package/contents/ui/main.qml:525
#, fuzzy, kde-format
#| msgid "Translucent"
msgctxt "@item:inmenu"
msgid "Translucent"
msgstr " Caurspīdīga"

#: package/contents/ui/main.qml:526
#, fuzzy, kde-format
#| msgid "Translucent"
msgctxt "@item:inmenu"
msgid "Translucent Light"
msgstr " Caurspīdīga"