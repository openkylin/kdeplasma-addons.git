# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Petros Vidalis <p_vidalis@hotmail.com>, 2010.
# jack gurulian <jack.gurulian@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-25 00:19+0000\n"
"PO-Revision-Date: 2010-09-15 13:40+0300\n"
"Last-Translator: jack gurulian <jack.gurulian@gmail.com>\n"
"Language-Team: American English <i18ngr@lists.hellug.gr>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: comic_package.cpp:20
#, kde-format
msgid "Images"
msgstr "Εικόνες"

#: comic_package.cpp:25
#, kde-format
msgid "Executable Scripts"
msgstr "Εκτελέσιμα σενάρια"

#: comic_package.cpp:29
#, kde-format
msgid "Main Script File"
msgstr "Κύριο αρχείο σεναρίου"