# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-13 00:44+0000\n"
"PO-Revision-Date: 2022-02-12 16:05+0100\n"
"Last-Translator: mkkDr2010 <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "General"
msgstr "Общи"

#: package/contents/config/config.qml:18
#, kde-format
msgctxt "@title"
msgid "Paths"
msgstr "Пътища"

#: package/contents/ui/ConfigGeneral.qml:49
#, kde-format
msgid "Change picture every:"
msgstr "Сменяне на снимката всеки:"

#: package/contents/ui/ConfigGeneral.qml:66
#, kde-format
msgid "Hours"
msgstr "Часове"

#: package/contents/ui/ConfigGeneral.qml:80
#, kde-format
msgid "Minutes"
msgstr "Минути"

#: package/contents/ui/ConfigGeneral.qml:94
#, kde-format
msgid "Seconds"
msgstr "Секунди"

#: package/contents/ui/ConfigGeneral.qml:104
#, kde-format
msgctxt "@label:listbox"
msgid "Image fill mode:"
msgstr "Режим на запълване на изображение:"

#: package/contents/ui/ConfigGeneral.qml:111
#, kde-format
msgctxt "@item:inlistbox"
msgid "Stretch"
msgstr "Разтегнато"

#: package/contents/ui/ConfigGeneral.qml:112
#, kde-format
msgctxt "@info"
msgid "The image is scaled to fit the frame"
msgstr "Изображението се мащабира, за да се побере в рамката"

#: package/contents/ui/ConfigGeneral.qml:116
#, kde-format
msgctxt "@item:inlistbox"
msgid "Preserve aspect fit"
msgstr "Запазване на пропорциите"

#: package/contents/ui/ConfigGeneral.qml:117
#, kde-format
msgctxt "@info"
msgid "The image is scaled uniformly to fit without cropping"
msgstr ""
"Изображението се мащабира равномерно, за да се побере, без да се изрязва"

#: package/contents/ui/ConfigGeneral.qml:121
#, kde-format
msgctxt "@item:inlistbox"
msgid "Preserve aspect crop"
msgstr "Запазване на пропорциите с изрязване"

#: package/contents/ui/ConfigGeneral.qml:122
#, kde-format
msgctxt "@info"
msgid "The image is scaled uniformly to fill, cropping if necessary"
msgstr ""
"Изображението се мащабира равномерно, за да се побере, като се изрязва, ако "
"е необходимо"

#: package/contents/ui/ConfigGeneral.qml:126
#, kde-format
msgctxt "@item:inlistbox"
msgid "Tile"
msgstr "Плочки"

#: package/contents/ui/ConfigGeneral.qml:127
#, kde-format
msgctxt "@info"
msgid "The image is duplicated horizontally and vertically"
msgstr "Изображението се повтаря хоризонтално и вертикално"

#: package/contents/ui/ConfigGeneral.qml:131
#, kde-format
msgctxt "@item:inlistbox"
msgid "Tile vertically"
msgstr "Плочки вертикално"

#: package/contents/ui/ConfigGeneral.qml:132
#, kde-format
msgctxt "@info"
msgid "The image is stretched horizontally and tiled vertically"
msgstr ""
"Изображението се разгъва хоризонтално и се разпределя като плочки вертикално"

#: package/contents/ui/ConfigGeneral.qml:136
#, kde-format
msgctxt "@item:inlistbox"
msgid "Tile horizontally"
msgstr "Плочки хоризонтално"

#: package/contents/ui/ConfigGeneral.qml:137
#, kde-format
msgctxt "@info"
msgid "The image is stretched vertically and tiled horizontally"
msgstr ""
"Изображението се разгъва вертикално и се разпределя като плочки хоризонтално"

#: package/contents/ui/ConfigGeneral.qml:141
#, kde-format
msgctxt "@item:inlistbox"
msgid "Pad"
msgstr "Подложка"

#: package/contents/ui/ConfigGeneral.qml:142
#, kde-format
msgctxt "@info"
msgid "The image is not transformed"
msgstr "Изображението не се трансформира"

#: package/contents/ui/ConfigGeneral.qml:187
#, kde-format
msgctxt "@label:checkbox"
msgid "General:"
msgstr "Общи:"

#: package/contents/ui/ConfigGeneral.qml:188
#, kde-format
msgctxt "@option:check"
msgid "Randomize order"
msgstr "Случаен ред"

#: package/contents/ui/ConfigGeneral.qml:193
#, kde-format
msgctxt "@option:check"
msgid "Pause slideshow when cursor is over image"
msgstr "Пауза на слайдшоуто, когато курсорът е над изображението"

#: package/contents/ui/ConfigGeneral.qml:198
#, kde-format
msgctxt "@option:check"
msgid "Show background frame"
msgstr "Показване на фонова рамка"

#: package/contents/ui/ConfigGeneral.qml:203
#, kde-format
msgctxt "@option:check"
msgid "Click on image to open in external application"
msgstr ""
"Отваряне на изображението във външно приложение при щракване върху "
"изображение"

#: package/contents/ui/ConfigPaths.qml:49
#, kde-format
msgctxt "@title:window"
msgid "Choose Files"
msgstr "Избиране на файлове"

#: package/contents/ui/ConfigPaths.qml:75
#, kde-format
msgctxt "@title:window"
msgid "Choose a Folder"
msgstr "Избиране на папка"

#: package/contents/ui/ConfigPaths.qml:123
#, kde-format
msgid "Remove path"
msgstr "Премахване на пътя"

#: package/contents/ui/ConfigPaths.qml:137
#, kde-format
msgctxt "@action:button"
msgid "Add Folder…"
msgstr "Добавяне на папка…"

#: package/contents/ui/ConfigPaths.qml:143
#, kde-format
msgctxt "@action:button"
msgid "Add Files…"
msgstr "Добавяне на файлове…"

#: package/contents/ui/main.qml:440
#, kde-format
msgctxt "@action:button"
msgid "Configure…"
msgstr "Конфигуриране…"